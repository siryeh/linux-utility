#!/bin/bash

KERNELVER=$(kernel-config list |grep "*" | awk '{print $2}' | replace linux- '')

if [ ! -z "$(uname -r | grep $KERNELVER)" -a -z "$1" ]; then
    echo Kernel is latest version $KERNELVER, no need update.
    exit
fi

KERNELCONFIG=$(ls -t /etc/kernels/ | head -n1)

genkernel --kernel-config=/etc/kernels/$KERNELCONFIG all

cd /boot
KERNELS=$(ls -t initramfs-genkernel-x86_64-* kernel-genkernel-x86_64-* System.map-genkernel-x86_64-*)
KERNELNUM=$(echo -e "$KERNELS" | wc -l)
if [ $KERNELNUM -gt 9 ]; then
    echo -e "$KERNELS" | tail -n3 | xargs rm 
fi

emerge -qv @x11-module-rebuild systemd networkmanager acpid virtualbox-modules